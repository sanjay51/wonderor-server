package wonderor.request;

import java.util.Arrays;
import java.util.Map;

public class Params {
    Path path;
    Map<String, String> querystring;
    Header header;

    public Params() {

    }

    public Map<String, String> getQuerystring() {
        return querystring;
    }

    public void setQuerystring(Map<String, String> querystring) {
        this.querystring = querystring;
    }

    @Override
    public String toString() {
        return "Params: " + Arrays.toString(this.querystring.entrySet().toArray());
    }
}
