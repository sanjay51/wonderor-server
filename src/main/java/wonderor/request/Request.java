package wonderor.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedHashMap;

@NoArgsConstructor
public class Request {
    @Getter @Setter LinkedHashMap<String, String> body;
    @Getter @Setter Params params;
    @Getter @Setter Context context;

    @Override
    public String toString() {
        return this.context.toString() +
                "\n --- \n" +
                this.params.toString() +
                "\n --- \n" +
                this.body.toString();
    }
}
