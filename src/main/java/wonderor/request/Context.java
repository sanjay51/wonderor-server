package wonderor.request;

public class Context {
    String httpMethod;

    public Context() {
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    @Override
    public String toString() {
        return this.httpMethod;
    }
}
