package wonderor;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import com.google.gson.Gson;
import wonderor.activities.ActivityFactory;
import wonderor.request.Request;
import wonderor.response.Response;

public class Main implements RequestHandler<Request, Response> {
    static final Gson gson = new Gson();

    public Response handleRequest(Request request, Context context) {

        System.out.println(gson.toJson(request));
        System.out.println(gson.toJson(context));

        final Response response;
        try {
            response = ActivityFactory.getInstance(request).getResponse();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        System.out.println("Final response: " + gson.toJson(response));
        return response;
    }
}