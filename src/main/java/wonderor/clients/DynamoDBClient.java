package wonderor.clients;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Index;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

import java.util.HashMap;
import java.util.Map;

public class DynamoDBClient {
    private static DynamoDBClient INSTANCE;
    private static AmazonDynamoDBClient client;
    private static DynamoDB dynamoDB;

    private DynamoDBClient() {
        client = new AmazonDynamoDBClient()
                .withRegion(Regions.US_WEST_2);
        dynamoDB = new DynamoDB(client);
    }

    public static DynamoDBClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DynamoDBClient();
        }

        return INSTANCE;
    }

    public Item getItemByPrimaryKey(String tableName,
                                    String primaryKeyName,
                                    String primaryKeyValue,
                                    String sortKey,
                                    String sortKeyValue) {
        final Table table = dynamoDB.getTable(tableName);

        final GetItemSpec getItemSpec = new GetItemSpec()
                .withPrimaryKey(primaryKeyName, primaryKeyValue, sortKey, sortKeyValue);

        return table.getItem(getItemSpec);
    }

    public Item getItemByPrimaryKey(String tableName,
                                    String primaryKeyName,
                                    String primaryKeyValue) {
        final Table table = dynamoDB.getTable(tableName);

        final GetItemSpec getItemSpec = new GetItemSpec()
                .withPrimaryKey(primaryKeyName, primaryKeyValue);

        return table.getItem(getItemSpec);
    }

    public ScanResult scan(final String tableName, final String filterAttName,
                           final String filterAttValue) {
        final ScanRequest scanRequest = new ScanRequest().withTableName(tableName);

        final Condition filterCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(filterAttValue));
        scanRequest.addScanFilterEntry(filterAttName, filterCondition);

        return client.scan(scanRequest);
    }

    public QueryResult query(final String tableName,
                             final String primaryKeyName, final String pkValue,
                             final String sortKeyName, final String skPrefixValue) {
        final QueryRequest queryRequest = new QueryRequest().withTableName(tableName);

        final Map<String, Condition> keyConditions = new HashMap<>();

        final Condition primaryKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(pkValue));

        keyConditions.put(primaryKeyName, primaryKeyCondition);

        final Condition sortKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.BEGINS_WITH)
                .withAttributeValueList(new AttributeValue().withS(skPrefixValue));

        keyConditions.put(sortKeyName, sortKeyCondition);

        queryRequest.setKeyConditions(keyConditions);

        return client.query(queryRequest);
    }

    public ItemCollection<QueryOutcome> queryIndex(final String tableName, final String indexName, final String primaryKeyName, final String pkValue) {
        final Table table = dynamoDB.getTable(tableName);
        final Index index = table.getIndex(indexName);

        final QuerySpec querySpec = new QuerySpec();
        querySpec.withKeyConditionExpression(primaryKeyName + " = :pkValue")
                .withValueMap(new ValueMap().withString(":pkValue", pkValue));

        return index.query(querySpec);
    }

    public void putItem(String tableName, Item item) {
        Table table = dynamoDB.getTable(tableName);
        table.putItem(item);
    }

    public void deleteItem(final String tableName,
                           final String primaryKeyName, final String pkValue,
                           final String sortKeyName, final String skValue) {
        final Table table = dynamoDB.getTable(tableName);
        PrimaryKey pk = new PrimaryKey(primaryKeyName, pkValue, sortKeyName, skValue);
        table.deleteItem(pk);
    }

    public Item updateItem(String tableName, String primaryKeyName, String primaryKeyValue,
                           String updateExpression, ValueMap valueMap) {
        Table table = dynamoDB.getTable(tableName);

        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey(primaryKeyName, primaryKeyValue)
                .withUpdateExpression(updateExpression)
                .withValueMap(valueMap)
                .withReturnValues(ReturnValue.UPDATED_NEW);

        return table.updateItem(updateItemSpec).getItem();
    }
}
