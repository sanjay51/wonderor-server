package wonderor.clients;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.athena.AmazonAthena;
import com.amazonaws.services.athena.AmazonAthenaClient;
import com.amazonaws.services.athena.AmazonAthenaClientBuilder;
import com.amazonaws.services.athena.model.GetQueryExecutionRequest;
import com.amazonaws.services.athena.model.GetQueryExecutionResult;
import com.amazonaws.services.athena.model.QueryExecutionContext;
import com.amazonaws.services.athena.model.QueryExecutionState;
import com.amazonaws.services.athena.model.ResultConfiguration;
import com.amazonaws.services.athena.model.StartQueryExecutionRequest;
import com.amazonaws.services.athena.model.StartQueryExecutionResult;
import wonderor.utils.Constants;

public class AthenaClient {
    private static AthenaClient INSTANCE;
    private static AmazonAthena client;

    private AthenaClient() {
        this.client = AmazonAthenaClientBuilder.standard()
                .withRegion(Constants.Config.REGION)
                .build();
    }

    public static AthenaClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AthenaClient();
        }

        return INSTANCE;
    }

    public String submitAthenaQuery(final String database, final String query, final String outputLocation) {
        final QueryExecutionContext queryExecutionContext = new QueryExecutionContext().withDatabase(database);

        final ResultConfiguration resultConfiguration = new ResultConfiguration()
                .withOutputLocation(outputLocation);

        final StartQueryExecutionRequest startQueryExecutionRequest = new StartQueryExecutionRequest()
                .withQueryString(query)
                .withQueryExecutionContext(queryExecutionContext)
                .withResultConfiguration(resultConfiguration);

        final StartQueryExecutionResult startQueryExecutionResult =
                client.startQueryExecution(startQueryExecutionRequest);

        return startQueryExecutionResult.getQueryExecutionId();
    }

    public void waitForQueryToComplete(final String queryExecutionId) throws InterruptedException {
        final GetQueryExecutionRequest getQueryExecutionRequest = new GetQueryExecutionRequest()
                .withQueryExecutionId(queryExecutionId);

        GetQueryExecutionResult getQueryExecutionResult;

        boolean isQueryStillRunning = true;
        while (isQueryStillRunning) {
            getQueryExecutionResult = client.getQueryExecution(getQueryExecutionRequest);
            String queryState = getQueryExecutionResult.getQueryExecution().getStatus().getState();

            if (queryState.equals(QueryExecutionState.FAILED.toString())) {
                throw new RuntimeException("Query Failed to run with Error Message: " + getQueryExecutionResult.getQueryExecution().getStatus().getStateChangeReason());
            } else if (queryState.equals(QueryExecutionState.CANCELLED.toString())) {
                throw new RuntimeException("Query was cancelled.");
            } else if (queryState.equals(QueryExecutionState.SUCCEEDED.toString())) {
                isQueryStillRunning = false;
            } else {
                // Sleep an amount of time before retrying again.
                Thread.sleep(500);
            }

            System.out.println("Athena query status: " + queryState);
        }
    }
}
