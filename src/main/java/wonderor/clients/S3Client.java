package wonderor.clients;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;

public class S3Client {
    private static S3Client INSTANCE;
    public static AmazonS3 client;

    private S3Client() {
        client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.US_WEST_2)
                .build();
    }

    public static S3Client getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new S3Client();
        }

        return INSTANCE;
    }

    public void putObject(final String bucketName, final String key, final String value) {
        this.client.putObject(bucketName, key, value);
    }

    public void copyObject(final String bucketName, final String oldKey, final String newKey) {
        this.client.copyObject(bucketName, oldKey, bucketName, newKey);
    }

    public void deleteObject(final String bucketName, final String key) {
        this.client.deleteObject(bucketName, key);
    }

    public S3Object getObject(final String bucketName, final String key) {
        return this.client.getObject(bucketName, key);
    }
}
