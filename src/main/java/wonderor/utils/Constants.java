package wonderor.utils;

import com.amazonaws.regions.Regions;

public class Constants {
    public static final String TYPE_ANSWER = "answer";
    public static final String ENTITY_ANSWER_TSID_PREFIX = "answer_";
    public static final String TYPE_QUESTION = "question";

    public static class Config {
        public static final Regions REGION = Regions.US_WEST_2;
    }

    public static class PopularEntitiesS3Config {
        public static final String ATHENA_DATABASE = "wonderor-datastream";

        public static final String ATHENA_QUERY = "SELECT entityId, tsId, type, authorId, " +
                "title, " +
                "content, " +
                "upvoteCount, downvoteCount, createdEpoch, lastUpdatedEpoch " +
                "FROM wonderor_data_stream order by upvoteCount desc";

        public static final String BUCKET = "wonderor-data-build";
        public static final String DIRECTORY = "popularEntities";
        public static final String ATHENA_OUTPUT_PATH = "s3://" + BUCKET + "/" + DIRECTORY + "/";
        public static final String LATEST_DATA_KEY = DIRECTORY + "/latest.csv";
    }
    public static final String PARAM_API = "api";
    public static final String SEPARATOR = "_";

    public static final String API_createEntity = "createEntity";
    public static final String API_createEntityMetadata = "createEntityMetadata";
    public static final String API_deleteEntityMetadata = "deleteEntityMetadata";
    public static final String API_getEntitiesByAuthor = "getEntitiesByAuthor";
    public static final String API_getEntityMetadatumByAuthor = "getEntityMetadatumByAuthor";
    public static final String API_getEntityById = "getEntityById";
    public static final String API_getEntitiesByType = "getEntitiesByType";
    public static final String API_getSubEntitiesByEntityId = "getSubEntitiesByEntityId";
    public static final String API_getPopularEntities = "getPopularEntities";
    public static final String API_getPopularStories = "getPopularStories";

    public static final String API_createUser = "createUser";
    public static final String API_getUserProfile = "getUserProfile";
    public static final String API_updateUserProfileAttributes = "updateUserProfileAttributes";

    // offline processing API
    public static final String API_refreshPopularEntityData = "refreshPopularEntityData";

    // user table
    public static final String table_user = "user";
    public static final String col_userSub = "userSub";
    public static final String col_fname = "fname";
    public static final String col_lname = "lname";
    public static final String col_email = "email";
    public static final String col_username = "username";

    // entity Table
    public static final String table_entity = "entity";
    public static final String col_entityId = "entityId";
    public static final String col_tsId = "tsId"; // typeSubEntityId
    public static final String col_authorId = "authorId";
    public static final String col_title = "title";
    public static final String col_content = "content";
    public static final String col_topics = "topics";
    public static final String col_upvoteCount = "upvoteCount";
    public static final String col_downvoteCount = "downvoteCount";
    public static final String col_rating = "rating";

    public static final String col_createdEpoch = "createdEpoch";
    public static final String col_lastUpdatedEpoch = "lastUpdatedEpoch";

    public static final String col_type = "type";
    public static final String col_subEntityId = "subEntityId";

    // unified entity
    public static final String col_authorImage = "authorImage";
    public static final String col_authorBio = "authorBio";

    // entity metadata table
    public static final String table_entity_metadata = "entity_metadata";
    public static final String col_parentEntityId = "parentEntityId";
    public static final String col_value = "value";
    public static final String col_entityType = "entityType";

    // events
    public static final String EVENT_remove = "REMOVE";
    public static final String EVENT_modify = "MODIFY";
    public static final String EVENT_insert = "INSERT";

    // event buckets
    public static final String EVENT_BUCKET_ENTITY = "wonderor-data-stream";
    public static final String EVENT_BUCKET_USER = "wonderor-dynamodb-events";
    public static final String EVENT_BUCKET_ENTITY_METADATA = "wonderor-dynamodb-events";

}