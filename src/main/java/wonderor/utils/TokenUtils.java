package wonderor.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class TokenUtils {
    private static String KEY = "3huUQGIB12V8oA863246532Oi9mJaT4s";
    private static String ISSUER = "ixtutor.net";
    private static String claim_ID = "id";

    public static String generateToken(String id) {
        String token = JWT.create()
                .withIssuer(ISSUER)
                .withClaim(claim_ID, id)
                .sign(Algorithm.HMAC256(KEY.getBytes()));

        return token;

    }

    public static void verifyToken(String token, String id) throws NoSuchProviderException, NoSuchAlgorithmException {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(KEY.getBytes()))
                .withIssuer(ISSUER)
                .withClaim(claim_ID, id)
                .build();
        verifier.verify(token);
    }
}
