package wonderor.utils;

import com.amazonaws.util.StringUtils;
import wonderor.exceptions.InvalidInputException;

import java.time.Instant;
import java.util.List;

public class Utils {
    public static boolean isBlank(final String input) {
        return StringUtils.isNullOrEmpty(StringUtils.trim(input));
    }

    public static void assertNotBlank(final String str, final String label) {
        if (isBlank(str)) {
            throw new InvalidInputException("Invalid " + label + ".");
        }
    }

    public static void assertNotEmpty(final List list, final String label) {
        if (list == null || list.size() == 0) {
            throw new InvalidInputException("Invalid " + label + ".");
        }
    }

    public static long getCurrentTimeMillis() {
        return Instant.now().toEpochMilli();
    }
}
