package wonderor.activities.processing;

import wonderor.activities.Activity;
import wonderor.clients.AthenaClient;
import wonderor.clients.S3Client;
import wonderor.request.Request;
import wonderor.response.Response;

import static wonderor.utils.Constants.PopularEntitiesS3Config.ATHENA_DATABASE;
import static wonderor.utils.Constants.PopularEntitiesS3Config.ATHENA_OUTPUT_PATH;
import static wonderor.utils.Constants.PopularEntitiesS3Config.BUCKET;
import static wonderor.utils.Constants.PopularEntitiesS3Config.DIRECTORY;
import static wonderor.utils.Constants.PopularEntitiesS3Config.LATEST_DATA_KEY;

public class RefreshPopularEntityDataActivity extends Activity {
    private static AthenaClient athenaClient = AthenaClient.getInstance();
    private static S3Client s3Client = S3Client.getInstance();

    private static String ATHENA_QUERY = getAthenaQuery();

    public RefreshPopularEntityDataActivity(final Request request) {
        // placeholder for request parameter processing
    }

    @Override
    public Response enact() throws Exception {

        // submit query
        System.out.println("Submitting Athena query.. " + ATHENA_QUERY);
        final String athenaQueryId = this.athenaClient.submitAthenaQuery(ATHENA_DATABASE, ATHENA_QUERY, ATHENA_OUTPUT_PATH);
        System.out.println("Submitted Athena query with Id: " + athenaQueryId);

        // wait for query to complete
        athenaClient.waitForQueryToComplete(athenaQueryId);

        // cleanup stuff
        final String currentKey = DIRECTORY + "/" + athenaQueryId + ".csv";
        cleanup(BUCKET, currentKey, LATEST_DATA_KEY);

        final Response response = new Response();
        response.setResponse(athenaQueryId);

        return response;
    }

    private void cleanup(final String bucket, final String currentKey, final String newKey) {

        // rename
        s3Client.copyObject(bucket, currentKey, newKey);

        // clear up meta-data
        s3Client.deleteObject(bucket, currentKey + ".metadata");
    }

    @Override
    public void validateRequest() {
    }

    private static String getAthenaQuery() {
        return String.join("\n",
                "with all_entities as (",
                "  SELECT entityId, tsId, type, authorId, title, content, ",
                "  createdEpoch, lastUpdatedEpoch",
                "  FROM wonderor_data_stream",
                "),",
                "",
                "votes as (",
                "  select entityId, value, count(*) as count from entitymetadata ",
                "  where type='vote'",
                "  group by entityId, value",
                "),",
                "",
                "agg_votes as (",
                "  select a.entityId, upvoteCount, downVoteCount from",
                "  (select entityId, count as upvoteCount from votes where value = '1') a",
                "  FULL JOIN",
                "  (select entityId, count as downvoteCount from votes where value = '-1') b",
                "  on a.entityId = b.entityId",
                "),",
                "",
                "questions as (",
                "  select * from all_entities where tsId = 'question' ",
                "  limit 5000",
                "),",
                "",
                "questions_with_votes as (",
                "  select questions.entityId, tsId, type, authorId, title, content, ",
                "  createdEpoch, lastUpdatedEpoch,",
                "  case when upvoteCount is null then 0 else upvoteCount end as upvoteCount, ",
                "  case when downvoteCount is null then 0 else downvoteCount end as downvoteCount",
                "  from questions ",
                "  left join agg_votes",
                "  on",
                "  questions.entityId = agg_votes.entityId",
                "),",
                "",
                "all_answers as (",
                "  select * from all_entities where tsId like 'answer_%' ",
                "  and entityId in (select entityId from questions)",
                "),",
                "",
                "all_answers_with_votes as (",
                "  select all_answers.entityId, tsId, type, authorId, title, content,",
                "  createdEpoch, lastUpdatedEpoch, ",
                "  case when upvoteCount is null then 0 else upvoteCount end as upvoteCount, ",
                "  case when downvoteCount is null then 0 else downvoteCount end as downvoteCount",
                "  from all_answers",
                "  left join",
                "  agg_votes",
                "  on all_answers.tsId = concat('answer_', agg_votes.entityId)",
                "),",
                "",
                "all_answers_with_rating as (",
                "  select *, (upvoteCount - downvoteCount) as rating",
                "  from all_answers_with_votes",
                "),",
                "",
                "top_answers as (",
                "  select * from (",
                "    select *, ROW_NUMBER() OVER (PARTITION BY tsId ORDER BY rating DESC) rank",
                "    from all_answers_with_rating",
                "  ) where rank = 1",
                "),",
                "",
                "entity_data as (",
                "select entityId, tsId, type, authorId, title, content, ",
                "  upvoteCount, downvoteCount, createdEpoch, lastUpdatedEpoch, rating",
                "  from (",
                "    select *, (upvoteCount - downvoteCount) as rating, 1 as rank from questions_with_votes ",
                "    union ",
                "    select * from top_answers)",
                "),",
                "",
                "users as (",
                "  select * from user",
                ")",
                "",
                "select entityId, tsId, type, authorId, title, content, upvoteCount, downvoteCount, rating, ",
                "entity_data.createdEpoch as createdEpoch, entity_data.lastUpdatedEpoch as lastUpdatedEpoch,",
                "username, 'notAvailable' as authorImage, '5+ years software development experience' as authorBio",
                "from entity_data",
                "inner join ",
                "users",
                "on entity_data.authorId = users.userSub ",
                "order by rating desc"
                );
    }
}
