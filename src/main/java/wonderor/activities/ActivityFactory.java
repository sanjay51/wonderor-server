package wonderor.activities;

import wonderor.activities.entity.CreateEntityActivity;
import wonderor.activities.entity.CreateEntityMetadataActivity;
import wonderor.activities.entity.DeleteEntityMetadataActivity;
import wonderor.activities.entity.GetEntitiesByAuthorActivity;
import wonderor.activities.entity.GetEntitiesByTypeActivity;
import wonderor.activities.entity.GetEntityByIdActivity;
import wonderor.activities.entity.GetEntityMetadatumByAuthorActivity;
import wonderor.activities.entity.GetPopularEntitiesActivity;
import wonderor.activities.entity.GetPopularStoriesActivity;
import wonderor.activities.entity.GetSubEntitiesByEntityIdActivity;
import wonderor.activities.processing.RefreshPopularEntityDataActivity;
import wonderor.activities.user.CreateUserActivity;
import wonderor.activities.user.GetUserProfileActivity;
import wonderor.activities.user.UpdateUserProfileAttributesActivity;
import wonderor.request.Request;

import static wonderor.utils.Constants.*;

public class ActivityFactory {
    public static Activity getInstance(final Request request) {
        final String API = request.getParams().getQuerystring().get(PARAM_API);

        switch (API) {

            case API_createEntity:
                return new CreateEntityActivity(request);

            case API_createEntityMetadata:
                return new CreateEntityMetadataActivity(request);

            case API_deleteEntityMetadata:
                return new DeleteEntityMetadataActivity(request);

            case API_getEntityMetadatumByAuthor:
                return new GetEntityMetadatumByAuthorActivity(request);

            case API_getEntityById:
                return new GetEntityByIdActivity(request);

            case API_getEntitiesByType:
                return new GetEntitiesByTypeActivity(request);

            case API_getSubEntitiesByEntityId:
                return new GetSubEntitiesByEntityIdActivity(request);

            case API_refreshPopularEntityData:
                return new RefreshPopularEntityDataActivity(request);

            case API_getPopularEntities:
                return new GetPopularEntitiesActivity(request);

            case API_getPopularStories:
                return new GetPopularStoriesActivity(request);

            case API_createUser:
                return new CreateUserActivity(request);

            case API_getEntitiesByAuthor:
                return new GetEntitiesByAuthorActivity(request);

            case API_getUserProfile:
                return new GetUserProfileActivity(request);

            case API_updateUserProfileAttributes:
                return new UpdateUserProfileAttributesActivity(request);

            default:
                throw new UnsupportedOperationException("API not supported.");
        }
    }
}