package wonderor.activities;

import wonderor.response.Response;
import wonderor.exceptions.InvalidInputException;

public abstract class Activity {
    public abstract Response enact() throws Exception;
    public abstract void validateRequest() throws InvalidInputException;

    public Response getResponse() throws Exception {
        this.validateRequest();
        return this.enact();
    }
}
