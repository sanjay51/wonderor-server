package wonderor.activities.user;

import com.amazonaws.services.dynamodbv2.document.Item;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import static wonderor.utils.Constants.*;
import static wonderor.utils.Utils.assertNotBlank;

public class CreateUserActivity extends Activity {
    private User user;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public CreateUserActivity(final Request request) {
        this.user = User.newInstanceForCreateRequest(request);
    }

    @Override
    public Response enact() {

        final Item item = new Item()
                .withPrimaryKey(col_userSub, user.getUserSub())
                .withString(col_fname, user.getFname())
                .withString(col_lname, user.getLname())
                .withString(col_username, user.getUsername())
                .withString(col_email, user.getEmail())
                .withLong(col_createdEpoch, user.getCreatedEpoch())
                .withLong(col_lastUpdatedEpoch, user.getLastUpdatedEpoch());

        dynamoDBClient.putItem(table_user, item);

        final Response response = new Response();
        response.setResponse(user);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(user.getUserSub(), "userSub");
        assertNotBlank(user.getFname(), "fname");
        assertNotBlank(user.getLname(), "lname");
        assertNotBlank(user.getUsername(), "username");
        assertNotBlank(user.getEmail(), "email");
    }
}
