package wonderor.activities.user;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.StreamRecord;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.csv.CSVRecord;
import wonderor.request.Request;
import wonderor.utils.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static wonderor.utils.Constants.col_createdEpoch;
import static wonderor.utils.Constants.col_email;
import static wonderor.utils.Constants.col_fname;
import static wonderor.utils.Constants.col_lastUpdatedEpoch;
import static wonderor.utils.Constants.col_lname;
import static wonderor.utils.Constants.col_userSub;
import static wonderor.utils.Constants.col_username;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String userSub;
    private String fname;
    private String lname;
    private String username;
    private String email;
    private long createdEpoch;
    private long lastUpdatedEpoch;

    final static List<String> VALID_USER_ATTRIBUTES =
            Arrays.asList(col_fname, col_lname, col_username, col_email,
                    col_createdEpoch, col_lastUpdatedEpoch); // userSub is excluded as it's primary identifier (and not attribute)

    final static Gson gson = new Gson();

    public User (final Map<String, String> queryParams) {
        this.userSub = queryParams.get(col_userSub);
        this.fname = queryParams.get(col_fname);
        this.lname = queryParams.get(col_lname);
        this.username = queryParams.get(col_username);
        this.email = queryParams.get(col_email);

        long epoch = Utils.getCurrentTimeMillis();
        this.createdEpoch = epoch;
        this.lastUpdatedEpoch = epoch;
    }

    public static boolean isValidUserAttribute(final String attribute) {
        return VALID_USER_ATTRIBUTES.stream().anyMatch(att -> att.equalsIgnoreCase(attribute));
    }

    public static User newInstanceForCreateRequest(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        return new User(queryParams);
    }

    public static User newInstanceFromStreamEvent(final StreamRecord record) {
        final Map<String, AttributeValue> image = record.getNewImage();

        final User user = User.builder()
                .userSub(image.get(col_userSub).getS())
                .fname(image.get(col_fname).getS())
                .lname(image.get(col_lname).getS())
                .username(image.get(col_username).getS())
                .email(image.get(col_email).getS())
                .createdEpoch(Long.valueOf(image.get(col_createdEpoch).getN()))
                .lastUpdatedEpoch(Long.valueOf(image.get(col_lastUpdatedEpoch).getN()))
                .build();

        return user;
    }

    public static User newInstanceFromDynamoDbItem(final Item item) {
        final User user = User.builder()
                .userSub(item.getString(col_userSub))
                .fname(item.getString(col_fname))
                .lname(item.getString(col_lname))
                .username(item.getString(col_username))
                .email(item.getString(col_email))
                .createdEpoch(item.getLong(col_createdEpoch))
                .lastUpdatedEpoch(Long.valueOf(item.getLong(col_lastUpdatedEpoch)))
                .build();

        return user;
    }

    public static User newInstanceFromCSVRecord(final CSVRecord csvRecord) {
        final UserBuilder userBuilder = User.builder();

        if (csvRecord.isMapped(col_userSub)) userBuilder.userSub(csvRecord.get(col_userSub));
        if (csvRecord.isMapped(col_fname)) userBuilder.fname(csvRecord.get(col_fname));
        if (csvRecord.isMapped(col_lname)) userBuilder.lname(csvRecord.get(col_lname));
        if (csvRecord.isMapped(col_username)) userBuilder.username(csvRecord.get(col_username));
        if (csvRecord.isMapped(col_email)) userBuilder.email(csvRecord.get(col_email));
        if (csvRecord.isMapped(col_createdEpoch)) userBuilder.createdEpoch(Long.valueOf(csvRecord.get(col_createdEpoch)));
        if (csvRecord.isMapped(col_lastUpdatedEpoch)) userBuilder.lastUpdatedEpoch(Long.valueOf(csvRecord.get(col_lastUpdatedEpoch)));

        return userBuilder.build();
    }

    public String toJSONString() {
        return gson.toJson(this);
    }

    public String getUniqueIdentifier() {
        return getUniqueIdentifier(this.getUserSub());
    }

    public static String getUniqueIdentifier(final String userSub) {
        return userSub;
    }
}
