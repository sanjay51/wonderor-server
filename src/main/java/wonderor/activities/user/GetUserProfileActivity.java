package wonderor.activities.user;

import com.amazonaws.services.dynamodbv2.document.Item;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import java.util.Map;

import static wonderor.utils.Constants.col_userSub;
import static wonderor.utils.Constants.table_user;
import static wonderor.utils.Utils.assertNotBlank;

public class GetUserProfileActivity extends Activity {
    private String userSub;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public GetUserProfileActivity(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        this.userSub = queryParams.get(col_userSub);
    }

    @Override
    public Response enact() {
        final Item item = dynamoDBClient.getItemByPrimaryKey(table_user, col_userSub, userSub);

        final User entity = User.newInstanceFromDynamoDbItem(item);

        final Response response = new Response();
        response.setResponse(entity);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(userSub, "userSub");
    }
}
