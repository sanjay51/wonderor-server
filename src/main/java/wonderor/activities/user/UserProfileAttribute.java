package wonderor.activities.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Getter
public class UserProfileAttribute {
    final String attributeName;
    final String attributeValue;

    public static List<UserProfileAttribute> getAllFromQueryParams(final Map<String, String> queryParams) {
        final List<UserProfileAttribute> attributes = new ArrayList<>();

        for (Map.Entry<String, String> entry: queryParams.entrySet()) {
            if (User.isValidUserAttribute(entry.getKey())) {
                attributes.add(new UserProfileAttribute(entry.getKey(), entry.getValue()));
            }
        }

        return attributes;
    }
}
