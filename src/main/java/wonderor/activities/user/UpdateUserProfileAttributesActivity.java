package wonderor.activities.user;

import com.amazonaws.services.dynamodbv2.document.Item;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;
import wonderor.utils.Utils;

import java.util.List;
import java.util.Map;

import static wonderor.utils.Constants.col_lastUpdatedEpoch;
import static wonderor.utils.Constants.col_userSub;
import static wonderor.utils.Constants.table_user;
import static wonderor.utils.Utils.assertNotBlank;
import static wonderor.utils.Utils.assertNotEmpty;

public class UpdateUserProfileAttributesActivity extends Activity {
    private String userSub;
    private List<UserProfileAttribute> attributes;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public UpdateUserProfileAttributesActivity(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        this.userSub = queryParams.get(col_userSub);
        this.attributes = UserProfileAttribute.getAllFromQueryParams(queryParams);
    }

    @Override
    public Response enact() {

        final Item item = dynamoDBClient.getItemByPrimaryKey(table_user, col_userSub, this.userSub);

        for (final UserProfileAttribute attribute: attributes) {
            item.withString(attribute.attributeName, attribute.attributeValue);
        }

        item.withLong(col_lastUpdatedEpoch, Utils.getCurrentTimeMillis());

        dynamoDBClient.putItem(table_user, item);

        final Response response = new Response();
        response.setResponse(attributes);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(userSub, "userSub");
        assertNotEmpty(attributes, "attributes");
    }
}
