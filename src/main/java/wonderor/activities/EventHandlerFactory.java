package wonderor.activities;

import wonderor.activities.event.EntityEventHandler;
import wonderor.activities.event.EntityMetadataEventHandler;
import wonderor.activities.event.UserEventHandler;

import static com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import static wonderor.utils.Constants.table_entity;
import static wonderor.utils.Constants.table_entity_metadata;
import static wonderor.utils.Constants.table_user;

public class EventHandlerFactory {
    public static EventHandler getInstance(final DynamodbStreamRecord streamRecord) {
        final String arn = streamRecord.getEventSourceARN();
        final String table = arn.split(":")[5].split("/")[1];
        final String eventType = streamRecord.getEventName();

        System.out.println("Handling '" + table + "' table related " + eventType + " event");

        switch (table) {
            case table_user:
                return new UserEventHandler(eventType, streamRecord.getDynamodb());

            case table_entity:
                return new EntityEventHandler(eventType, streamRecord.getDynamodb());

            case table_entity_metadata:
                return new EntityMetadataEventHandler(eventType, streamRecord.getDynamodb());

            default:
                throw new UnsupportedOperationException("Events for '" + table + "' not supported.");
        }
    }
}