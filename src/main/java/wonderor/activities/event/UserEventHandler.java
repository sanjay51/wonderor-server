package wonderor.activities.event;

import com.amazonaws.services.dynamodbv2.model.StreamRecord;
import lombok.AllArgsConstructor;
import lombok.Getter;
import wonderor.activities.EventHandler;
import wonderor.activities.user.User;
import wonderor.clients.S3Client;

import static wonderor.utils.Constants.EVENT_BUCKET_USER;
import static wonderor.utils.Constants.col_userSub;

@AllArgsConstructor
@Getter
public class UserEventHandler extends EventHandler {
    final String eventType;
    final StreamRecord streamRecord;

    @Override
    protected void handleInsertRecord(final StreamRecord record) {
        final User user = User.newInstanceFromStreamEvent(record);

        S3Client.getInstance().putObject(EVENT_BUCKET_USER, "user/" + user.getUniqueIdentifier(),
                user.toJSONString());
    }

    @Override
    protected void handleRemoveRecord(final StreamRecord record) {
        final String key = User.getUniqueIdentifier(record.getKeys().get(col_userSub).getS());
        S3Client.getInstance().deleteObject(EVENT_BUCKET_USER, "user/" + key);
    }
}
