package wonderor.activities.event;

import com.amazonaws.services.dynamodbv2.model.StreamRecord;
import lombok.AllArgsConstructor;
import lombok.Getter;
import wonderor.activities.EventHandler;
import wonderor.activities.entity.Entity;
import wonderor.activities.entity.models.EntityMetadata;
import wonderor.clients.S3Client;

import static wonderor.utils.Constants.EVENT_BUCKET_ENTITY;
import static wonderor.utils.Constants.EVENT_BUCKET_ENTITY_METADATA;
import static wonderor.utils.Constants.col_entityId;
import static wonderor.utils.Constants.col_tsId;

@AllArgsConstructor
@Getter
public class EntityMetadataEventHandler extends EventHandler {
    final String eventType;
    final StreamRecord streamRecord;

    @Override
    protected void handleInsertRecord(final StreamRecord record) {
        final EntityMetadata entity = EntityMetadata.newInstanceFromStreamEvent(record);

        S3Client.getInstance().putObject(EVENT_BUCKET_ENTITY_METADATA, "entityMetadata/" + entity.getUniqueIdentifier(),
                entity.toJSONString());
    }

    @Override
    protected void handleRemoveRecord(final StreamRecord record) {
        final String key = Entity.getUniqueIdentifier(record.getKeys().get(col_entityId).getS(),
                record.getKeys().get(col_tsId).getS());
        S3Client.getInstance().deleteObject(EVENT_BUCKET_ENTITY, "entityMetadata/" + key);
    }
}
