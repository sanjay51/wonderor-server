package wonderor.activities;

import com.amazonaws.services.dynamodbv2.model.StreamRecord;

import static wonderor.utils.Constants.EVENT_insert;
import static wonderor.utils.Constants.EVENT_modify;
import static wonderor.utils.Constants.EVENT_remove;

public abstract class EventHandler {
    protected abstract void handleInsertRecord(final StreamRecord record);
    protected abstract void handleRemoveRecord(final StreamRecord record);
    protected abstract String getEventType();
    protected abstract StreamRecord getStreamRecord();

    public void handle() {
        boolean isSuccess = false;

        for (int i = 0; i < 3 && !isSuccess; i++) {
            try {
                System.out.println("Attempt #" + i);

                switch (getEventType()) {
                    case EVENT_insert:
                        handleInsertRecord(getStreamRecord());
                        break;

                    case EVENT_modify:
                        handleInsertRecord(getStreamRecord());
                        break;

                    case EVENT_remove:
                        handleRemoveRecord(getStreamRecord());
                        break;

                    default:
                        throw new RuntimeException("Unknown Entity event type: " + getEventType());
                }

                isSuccess = true;
            } catch (final Exception e) {
                System.out.println("Caught exception:" + e);
            }
        }
    }
}
