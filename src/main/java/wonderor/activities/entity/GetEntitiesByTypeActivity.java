package wonderor.activities.entity;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static wonderor.utils.Constants.*;
import static wonderor.utils.Utils.assertNotBlank;

public class GetEntitiesByTypeActivity extends Activity {
    private String type;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public GetEntitiesByTypeActivity(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        this.type = queryParams.get(col_type);
    }

    @Override
    public Response enact() {
        final ScanResult scanResult = dynamoDBClient
                .scan(table_entity, col_tsId, type);

        final List<Entity> entities = new ArrayList<>();

        for (final Map<String, AttributeValue> item: scanResult.getItems()) {
            final Entity entity = Entity.builder()
                    .entityId(item.get(col_entityId).getS())
                    .tsId(item.get(col_tsId).getS())
                    .type(type)
                    .authorId(item.get(col_authorId).getS())
                    .title(item.get(col_title).getS())
                    .content(item.get(col_content).getS())
                    .topics(item.get(col_topics) == null ? new ArrayList<>() : item.get(col_topics).getSS())
                    .createdEpoch(Long.valueOf(item.get(col_createdEpoch).getN()))
                    .lastUpdatedEpoch(Long.valueOf(item.get(col_lastUpdatedEpoch).getN()))
                    .build();

            entities.add(entity);
        }

        final Response response = new Response();
        response.setResponse(entities);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(type, "type");
    }
}
