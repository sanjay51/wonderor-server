package wonderor.activities.entity.models;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.StreamRecord;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import wonderor.request.Request;
import wonderor.utils.Utils;

import java.util.Map;

import static wonderor.utils.Constants.SEPARATOR;
import static wonderor.utils.Constants.col_authorId;
import static wonderor.utils.Constants.col_createdEpoch;
import static wonderor.utils.Constants.col_entityId;
import static wonderor.utils.Constants.col_entityType;
import static wonderor.utils.Constants.col_lastUpdatedEpoch;
import static wonderor.utils.Constants.col_parentEntityId;
import static wonderor.utils.Constants.col_tsId;
import static wonderor.utils.Constants.col_type;
import static wonderor.utils.Constants.col_value;
import static wonderor.utils.Utils.assertNotBlank;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class
EntityMetadata {
    public static final String TYPE_VOTE = "vote";
    public static final String VOTE_TSID__PREFIX = "vote_";

    private final String entityId; // primary key
    private final String parentEntityId;
    private final String tsId; // sort key

    private final String entityType;
    private final String type;
    private final String value;
    private final String authorId;

    long createdEpoch;
    long lastUpdatedEpoch;

    public static EntityMetadata newInstanceForCreateRequest(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        final EntityMetadata entityMetadata = EntityMetadata.builder()
                .entityId(queryParams.get(col_entityId))
                .parentEntityId(queryParams.get(col_parentEntityId))
                .tsId(generateTsId(queryParams.get(col_type), queryParams.get(col_authorId)))
                .entityType(queryParams.get(col_entityType))
                .type(queryParams.get(col_type))
                .value(queryParams.get(col_value))
                .authorId(queryParams.get(col_authorId))
                .createdEpoch(Utils.getCurrentTimeMillis())
                .lastUpdatedEpoch(Utils.getCurrentTimeMillis())
                .build();

        return entityMetadata;
    }

    public static EntityMetadata newInstanceFromStreamEvent(final StreamRecord record) {
        final Map<String, AttributeValue> image = record.getNewImage();

        final EntityMetadata entityMetadata = EntityMetadata.builder()
                .entityId(image.get(col_entityId).getS())
                .parentEntityId(image.get(col_parentEntityId).getS())
                .tsId(image.get(col_tsId).getS())
                .entityType(image.get(col_entityType).getS())
                .type(image.get(col_type).getS())
                .value(image.get(col_value).getS())
                .authorId(image.get(col_authorId).getS())
                .createdEpoch(Long.valueOf(image.get(col_createdEpoch).getN()))
                .lastUpdatedEpoch(Long.valueOf(image.get(col_lastUpdatedEpoch).getN()))
                .build();

        return entityMetadata;
    }

    public static EntityMetadata newInstanceFromDynamoDbItem(final Item item) {
        return EntityMetadata.builder()
                .entityId(item.getString(col_entityId))
                .parentEntityId(item.getString(col_parentEntityId))
                .tsId(item.getString(col_tsId))
                .entityType(item.getString(col_entityType))
                .type(item.getString(col_type))
                .value(item.getString(col_value))
                .authorId(item.getString(col_authorId))
                .createdEpoch(item.getLong(col_createdEpoch))
                .lastUpdatedEpoch(item.getLong(col_lastUpdatedEpoch))
                .build();
    }

    public Item asDynamoDbItem() {
        final Item item = new Item()
                .withPrimaryKey(col_entityId, this.getEntityId(), col_tsId, this.getTsId())
                .withString(col_parentEntityId, this.parentEntityId)
                .withString(col_entityType, this.getEntityType())
                .withString(col_type, this.getType())
                .withString(col_value, this.getValue())
                .withString(col_authorId, this.getAuthorId())
                .withLong(col_createdEpoch, this.getCreatedEpoch())
                .withLong(col_lastUpdatedEpoch, this.getLastUpdatedEpoch());

        return item;
    }

    public void validate() {
        assertNotBlank(this.getEntityId(), "entityId");
        assertNotBlank(this.getTsId(), "tsId");
        assertNotBlank(this.getEntityType(), "entityType");
        assertNotBlank(this.getType(), "type");
        assertNotBlank(this.getValue(), "value");
        assertNotBlank(this.getAuthorId(), "authorId");
        assertNotBlank(this.getAuthorId(), "authorId");
    }

    public static String generateTsId(final String type, final String authorId) {
        return type + SEPARATOR + authorId;
    }

    public String toJSONString() {
        Gson gson = new Gson();

        return gson.toJson(this);
    }

    public String getUniqueIdentifier() {
        return getUniqueIdentifier(this.getEntityId(), this.getTsId());
    }

    public static String getUniqueIdentifier(final String entityId, final String tsId) {
        return entityId + "_" + tsId;
    }
}
