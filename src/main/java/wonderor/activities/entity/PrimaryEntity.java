package wonderor.activities.entity;

import java.util.Map;
import java.util.UUID;

public class PrimaryEntity extends Entity {

    public PrimaryEntity(final Map<String, String> queryParams) {
        super(queryParams);
    }

    @Override
    protected void populateFirstTimeKeyFields() {
        this.setEntityId(UUID.randomUUID().toString());
        this.setTsId(this.getType());
    }

    @Override
    public String getTsId() {
        return this.getType();
    }
}
