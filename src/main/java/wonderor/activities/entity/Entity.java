package wonderor.activities.entity;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.StreamRecord;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.csv.CSVRecord;
import wonderor.request.Request;
import wonderor.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static wonderor.utils.Constants.SEPARATOR;
import static wonderor.utils.Constants.col_authorBio;
import static wonderor.utils.Constants.col_authorId;
import static wonderor.utils.Constants.col_authorImage;
import static wonderor.utils.Constants.col_content;
import static wonderor.utils.Constants.col_createdEpoch;
import static wonderor.utils.Constants.col_downvoteCount;
import static wonderor.utils.Constants.col_entityId;
import static wonderor.utils.Constants.col_lastUpdatedEpoch;
import static wonderor.utils.Constants.col_rating;
import static wonderor.utils.Constants.col_title;
import static wonderor.utils.Constants.col_topics;
import static wonderor.utils.Constants.col_tsId;
import static wonderor.utils.Constants.col_type;
import static wonderor.utils.Constants.col_upvoteCount;
import static wonderor.utils.Constants.col_username;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Entity {
    public static final String ENTITY_QUESTION = "question";
    public static final String ENTITY_ANSWER_TSID_PREFIX = "answer_";

    private String entityId; // primary key
    private String tsId; // primary key

    final String type; // sort key
    final String authorId;
    final String title;
    final String content;
    final List<String> topics;

    long createdEpoch;
    long lastUpdatedEpoch;

    Entity(final Map<String, String> queryParams) {
        this.entityId =
                queryParams.get(col_entityId); // can be null
        this.tsId = queryParams.get(col_tsId); // can be null

        this.type = queryParams.get(col_type);
        this.authorId = queryParams.get(col_authorId);
        this.title = cleanString(queryParams.get(col_title));
        this.content = cleanString(queryParams.get(col_content));
        this.topics = new ArrayList<>(); // TODO: Fill topic list
    }

    public static Entity newInstanceFromDynamoDbItem(final Item item) {
        return Entity.builder()
                .entityId(item.getString(col_entityId))
                .tsId(item.getString(col_tsId))
                .type(item.getString(col_type))
                .authorId(item.getString(col_authorId))
                .title(item.getString(col_title))
                .content(item.getString(col_content))
                .topics(item.getList(col_topics))
                .createdEpoch(item.getLong(col_createdEpoch))
                .lastUpdatedEpoch(item.getLong(col_lastUpdatedEpoch))
                .build();
    }

    public static Entity newInstanceForCreateRequest(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        final String type = queryParams.get(col_type);

        Entity entity;
        if (Entity.isPrimaryEntityType(type)) {
            entity = new PrimaryEntity(queryParams);
        } else {
            entity = new SubEntity(queryParams);
        }

        entity.populateFirstTimeFields();

        return entity;

    }

    public static Entity newInstanceFromCSVRecord(final CSVRecord csvRecord) {
        return new Entity(
                csvRecord.get(col_entityId),
                csvRecord.get(col_tsId),
                csvRecord.get(col_type),
                csvRecord.get(col_authorId),
                csvRecord.get(col_title),
                csvRecord.get(col_content),
                new ArrayList<>(),
                Long.valueOf(csvRecord.get(col_createdEpoch)),
                Long.valueOf(csvRecord.get(col_lastUpdatedEpoch))
        );
    }

    public static Entity newInstanceFromStreamEvent(final StreamRecord record) {
        final Map<String, AttributeValue> image = record.getNewImage();

        final Entity entity = new Entity(
                image.get(col_entityId).getS(),
                image.get(col_tsId).getS(),
                image.get(col_type).getS(),
                image.get(col_authorId).getS(),
                image.get(col_title).getS(),
                image.get(col_content).getS(),
                image.get(col_topics).getSS(),
                Long.valueOf(image.get(col_createdEpoch).getN()),
                Long.valueOf(image.get(col_lastUpdatedEpoch).getN())
        );

        return entity;
    }

    private void populateFirstTimeFields() {

        long epoch = Utils.getCurrentTimeMillis();
        this.createdEpoch = epoch;
        this.lastUpdatedEpoch = epoch;

        this.populateFirstTimeKeyFields();
    }

    protected void populateFirstTimeKeyFields() {
        // to be implemented by inherited classes
    }

    private static boolean isPrimaryEntityType(final String type) {
        return type.equalsIgnoreCase(ENTITY_QUESTION);
    }

    public static String generateTsId(final String type, final String subEntityId) {
        if (isPrimaryEntityType(type)) return type;

        return type + SEPARATOR + subEntityId;
    }

    private String cleanString(final String original) {
        return original.replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;");
    }

    public String toJSONString() {
        Gson gson = new Gson();

        return gson.toJson(this);
    }

    public String getUniqueIdentifier() {
        return getUniqueIdentifier(this.getEntityId(), this.getTsId());
    }

    public static String getUniqueIdentifier(final String entityId, final String tsId) {
        return entityId + "_" + tsId;
    }

    @AllArgsConstructor
    @Builder
    @Getter
    public static class UnifiedEntity {
        private String entityId; // primary key
        private String tsId;

        final String type;
        final String authorId;
        final String title;
        final String content;
        final long upvoteCount;
        final long downvoteCount;
        final float rating;
        final String username;
        final String authorImage;
        final String authorBio;

        final long createdEpoch;
        final long lastUpdatedEpoch;

        public static UnifiedEntity newInstanceFromCSVRecord(final CSVRecord csvRecord) {
            return UnifiedEntity.builder()
                    .entityId(csvRecord.get(col_entityId))
                    .tsId(csvRecord.get(col_tsId))
                    .type(csvRecord.get(col_type))
                    .authorId(csvRecord.get(col_authorId))
                    .title(csvRecord.get(col_title))
                    .content(csvRecord.get(col_content))
                    .upvoteCount(Long.parseLong(csvRecord.get(col_upvoteCount)))
                    .downvoteCount(Long.parseLong(csvRecord.get(col_downvoteCount)))
                    .rating(Float.parseFloat(csvRecord.get(col_rating)))
                    .username(csvRecord.get(col_username))
                    .authorImage(csvRecord.get(col_authorImage))
                    .authorBio(csvRecord.get(col_authorBio))
                    .createdEpoch(Long.parseLong(csvRecord.get(col_createdEpoch)))
                    .lastUpdatedEpoch(Long.parseLong(csvRecord.get(col_lastUpdatedEpoch)))
                    .build();
        }

    }
}
