package wonderor.activities.entity;

import com.amazonaws.services.dynamodbv2.document.Item;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import static wonderor.utils.Constants.*;
import static wonderor.utils.Utils.assertNotBlank;

public class CreateEntityActivity extends Activity {
    private Entity entity;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public CreateEntityActivity(final Request request) {
        this.entity = Entity.newInstanceForCreateRequest(request);
    }

    @Override
    public Response enact() {

        final Item item = new Item()
                .withPrimaryKey(col_entityId, entity.getEntityId(), col_tsId, entity.getTsId())
                .withString(col_type, entity.getType())
                .withString(col_authorId, entity.getAuthorId())
                .withString(col_title, entity.getTitle())
                .withString(col_content, entity.getContent())
                .withList(col_topics, entity.getTopics())
                .withLong(col_createdEpoch, entity.getCreatedEpoch())
                .withLong(col_lastUpdatedEpoch, entity.getLastUpdatedEpoch());

        dynamoDBClient.putItem(table_entity, item);

        final Response response = new Response();
        response.setResponse(entity);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(entity.getAuthorId(), "author");
        assertNotBlank(entity.getTitle(), "question");
        assertNotBlank(entity.getContent(), "question details");
    }
}
