package wonderor.activities.entity;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;
import wonderor.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static wonderor.utils.Constants.col_authorId;
import static wonderor.utils.Constants.col_content;
import static wonderor.utils.Constants.col_createdEpoch;
import static wonderor.utils.Constants.col_entityId;
import static wonderor.utils.Constants.col_lastUpdatedEpoch;
import static wonderor.utils.Constants.col_title;
import static wonderor.utils.Constants.col_topics;
import static wonderor.utils.Constants.col_tsId;
import static wonderor.utils.Constants.col_type;
import static wonderor.utils.Constants.table_entity;
import static wonderor.utils.Utils.assertNotBlank;

public class GetSubEntitiesByEntityIdActivity extends Activity {
    private String entityId;
    private String type;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public GetSubEntitiesByEntityIdActivity(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        this.entityId = queryParams.get(col_entityId);
        this.type = queryParams.get(col_type);
    }

    @Override
    public Response enact() {
        final String tsIdPrefix = type + Constants.SEPARATOR;
        final QueryResult queryResult = dynamoDBClient
                .query(table_entity, col_entityId, entityId, col_tsId, tsIdPrefix);

        final List<Entity> entities = new ArrayList<>();

        for (final Map<String, AttributeValue> item: queryResult.getItems()) {
            final Entity entity = Entity.builder()
                    .entityId(item.get(col_entityId).getS())
                    .tsId(item.get(col_tsId).getS())
                    .type(type)
                    .authorId(item.get(col_authorId).getS())
                    .title(item.get(col_title).getS())
                    .content(item.get(col_content).getS())
                    .topics(item.get(col_topics) == null ? new ArrayList<>() : item.get(col_topics).getSS())
                    .createdEpoch(Long.valueOf(item.get(col_createdEpoch).getN()))
                    .lastUpdatedEpoch(Long.valueOf(item.get(col_lastUpdatedEpoch).getN()))
                    .build();

            entities.add(entity);
        }

        final Response response = new Response();
        response.setResponse(entities);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(entityId, "entity Id");
        assertNotBlank(type, "type");
    }
}
