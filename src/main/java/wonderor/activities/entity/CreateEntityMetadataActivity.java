package wonderor.activities.entity;

import com.amazonaws.services.dynamodbv2.document.Item;
import wonderor.activities.Activity;
import wonderor.activities.entity.models.EntityMetadata;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import static wonderor.utils.Constants.table_entity_metadata;

public class CreateEntityMetadataActivity extends Activity {
    private EntityMetadata entityMetadata;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public CreateEntityMetadataActivity(final Request request) {
        this.entityMetadata = EntityMetadata.newInstanceForCreateRequest(request);
    }

    @Override
    public Response enact() {

        // create dynamodb item
        final Item item = this.entityMetadata.asDynamoDbItem();

        // write to dynamodb
        dynamoDBClient.putItem(table_entity_metadata, item);

        // return response
        final Response response = new Response();
        response.setResponse(entityMetadata);

        return response;
    }

    @Override
    public void validateRequest() {
        this.entityMetadata.validate();
    }
}
