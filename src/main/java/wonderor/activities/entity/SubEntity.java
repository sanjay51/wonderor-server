package wonderor.activities.entity;

import java.util.Map;
import java.util.UUID;

public class SubEntity extends Entity {
    public SubEntity(final Map<String, String> queryParams) {
        super(queryParams);
    }

    @Override
    protected void populateFirstTimeKeyFields() {
        this.setTsId(generateTsId(this.getType(), UUID.randomUUID().toString()));
    }
}
