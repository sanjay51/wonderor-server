package wonderor.activities.entity;

import wonderor.activities.Activity;
import wonderor.activities.entity.models.EntityMetadata;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import java.util.Map;

import static wonderor.utils.Constants.col_authorId;
import static wonderor.utils.Constants.col_entityId;
import static wonderor.utils.Constants.col_tsId;
import static wonderor.utils.Constants.col_type;
import static wonderor.utils.Constants.table_entity_metadata;
import static wonderor.utils.Utils.assertNotBlank;

public class DeleteEntityMetadataActivity extends Activity {
    private final String entityId;
    private final String type;
    private final String authorId;

    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public DeleteEntityMetadataActivity(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        this.entityId = queryParams.get(col_entityId);
        this.type = queryParams.get(col_type);
        this.authorId = queryParams.get(col_authorId);
    }

    @Override
    public Response enact() {
        final String tsId = EntityMetadata.generateTsId(type, authorId);
        System.out.println("Operation DeleteEntityMetadata with entityId:" + entityId + "; tsId:" + tsId);

        // delete from dynamodb
        dynamoDBClient.deleteItem(table_entity_metadata, col_entityId, entityId, col_tsId, tsId);

        // return response
        final Response response = new Response();
        response.setResponse("Deleted");

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(this.entityId, "entityId");
        assertNotBlank(this.type, "type");
        assertNotBlank(this.authorId, "authorId");
    }
}
