package wonderor.activities.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.csv.CSVRecord;

import static wonderor.activities.entity.Entity.UnifiedEntity;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Story {
    UnifiedEntity question;
    UnifiedEntity answer;

    public static Story newInstanceFromCSVRecord(final StoryCSVRecord storyCSVRecord) {
        final UnifiedEntity question = UnifiedEntity.newInstanceFromCSVRecord(storyCSVRecord.question);
        final UnifiedEntity answer = UnifiedEntity.newInstanceFromCSVRecord(storyCSVRecord.answer);

        return Story.builder()
                .question(question)
                .answer(answer)
                .build();
    }

    @Builder
    public static class StoryCSVRecord {
        CSVRecord question;
        CSVRecord answer;
    }
}
