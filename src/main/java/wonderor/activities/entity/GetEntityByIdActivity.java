package wonderor.activities.entity;

import com.amazonaws.services.dynamodbv2.document.Item;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import java.util.Map;

import static wonderor.utils.Constants.*;
import static wonderor.utils.Utils.assertNotBlank;

public class GetEntityByIdActivity extends Activity {
    private String entityId;
    private String tsId;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public GetEntityByIdActivity(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        this.entityId = queryParams.get(col_entityId);

        final String type = queryParams.get(col_type);
        final String subEntityId = queryParams.get(col_subEntityId);
        this.tsId = Entity.generateTsId(type, subEntityId);
    }

    @Override
    public Response enact() {
        final Item item = dynamoDBClient.getItemByPrimaryKey(table_entity, col_entityId, entityId, col_tsId, tsId);

        final Entity entity = Entity.builder()
                .entityId(entityId)
                .tsId(item.getString(tsId))
                .type(item.getString(col_type))
                .authorId(item.getString(col_authorId))
                .title(item.getString(col_title))
                .content(item.getString(col_content))
                .topics(item.getList(col_topics))
                .createdEpoch(item.getLong(col_createdEpoch))
                .lastUpdatedEpoch(item.getLong(col_lastUpdatedEpoch))
                .build();

        final Response response = new Response();
        response.setResponse(entity);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(entityId, "entity Id");
        assertNotBlank(tsId, "type or subEntityId");
    }
}
