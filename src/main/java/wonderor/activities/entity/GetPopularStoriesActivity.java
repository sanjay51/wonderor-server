package wonderor.activities.entity;

import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import wonderor.activities.Activity;
import wonderor.clients.S3Client;
import wonderor.request.Request;
import wonderor.response.Response;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wonderor.activities.entity.Entity.ENTITY_ANSWER_TSID_PREFIX;
import static wonderor.activities.entity.Entity.ENTITY_QUESTION;
import static wonderor.activities.entity.Story.StoryCSVRecord;
import static wonderor.utils.Constants.PopularEntitiesS3Config.BUCKET;
import static wonderor.utils.Constants.PopularEntitiesS3Config.LATEST_DATA_KEY;
import static wonderor.utils.Constants.col_entityId;
import static wonderor.utils.Constants.col_tsId;

public class GetPopularStoriesActivity extends Activity {
    private S3Client s3Client = S3Client.getInstance();

    public GetPopularStoriesActivity(final Request request) {

    }

    @Override
    public Response enact() throws Exception {
        final S3Object s3Object = s3Client.getObject(BUCKET, LATEST_DATA_KEY);

        final List<Story> stories = new ArrayList<>();

        final BufferedReader reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));

        final Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withFirstRecordAsHeader().parse(reader);

        final List<StoryCSVRecord> storyCSVRecords = convertToStoryCSVRecordsList(records);

        for (final StoryCSVRecord storyCSVRecord: storyCSVRecords) {
            final Story story = Story.newInstanceFromCSVRecord(storyCSVRecord);

            stories.add(story);
        }

        final Response response = new Response();
        response.setResponse(stories);

        return response;
    }

    private List<StoryCSVRecord> convertToStoryCSVRecordsList(Iterable<CSVRecord> records) {
        final List<StoryCSVRecord> storyCSVRecords = new ArrayList<>();

        final List<CSVRecord> questionCSVRecords = new ArrayList<>();
        final Map<String, CSVRecord> answerCSVRecords = new HashMap<>();

        // separate question and answer csv records
        for (final CSVRecord record: records) {
            final String tsId = record.get(col_tsId);

            if (tsId != null) {
                if (tsId.startsWith(ENTITY_ANSWER_TSID_PREFIX)) {
                    answerCSVRecords.put(record.get(col_entityId), record);
                } else if (tsId.startsWith(ENTITY_QUESTION)) {
                    questionCSVRecords.add(record);
                }
            }
        }

        // create story csv objects
        for (final CSVRecord questionCSVRecord: questionCSVRecords) {
            final CSVRecord answerCSVRecord = answerCSVRecords.get(questionCSVRecord.get(col_entityId));

            StoryCSVRecord storyCSVRecord = StoryCSVRecord.builder()
                    .question(questionCSVRecord)
                    .answer(answerCSVRecord)
                    .build();

            storyCSVRecords.add(storyCSVRecord);
        }

        return storyCSVRecords;
    }

    @Override
    public void validateRequest() {
    }
}
