package wonderor.activities.entity;

import com.amazonaws.services.kinesisanalytics.model.CSVMappingParameters;
import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import wonderor.activities.Activity;
import wonderor.clients.S3Client;
import wonderor.request.Request;
import wonderor.response.Response;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static wonderor.utils.Constants.PopularEntitiesS3Config.BUCKET;
import static wonderor.utils.Constants.PopularEntitiesS3Config.LATEST_DATA_KEY;

public class GetPopularEntitiesActivity extends Activity {
    private S3Client s3Client = S3Client.getInstance();

    public GetPopularEntitiesActivity(final Request request) {

    }

    @Override
    public Response enact() throws Exception {
        final S3Object s3Object = s3Client.getObject(BUCKET, LATEST_DATA_KEY);

        final List<Entity> entities = new ArrayList<>();

        final BufferedReader reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));

        Iterable<CSVRecord> records = CSVFormat.DEFAULT
                .withFirstRecordAsHeader().parse(reader);

        for (CSVRecord record: records) {
            final Entity entity = Entity.newInstanceFromCSVRecord(record);
            entities.add(entity);
        }

        final Response response = new Response();
        response.setResponse(entities);

        return response;
    }

    @Override
    public void validateRequest() {
    }
}
