package wonderor.activities.entity;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import wonderor.activities.Activity;
import wonderor.clients.DynamoDBClient;
import wonderor.request.Request;
import wonderor.response.Response;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static wonderor.utils.Constants.col_authorId;
import static wonderor.utils.Constants.table_entity;
import static wonderor.utils.Utils.assertNotBlank;

public class GetEntitiesByAuthorActivity extends Activity {
    private String authorId;
    private DynamoDBClient dynamoDBClient = DynamoDBClient.getInstance();

    public GetEntitiesByAuthorActivity(final Request request) {
        final Map<String, String> queryParams = request.getParams().getQuerystring();

        this.authorId = queryParams.get(col_authorId);
    }

    @Override
    public Response enact() {
        final ItemCollection<QueryOutcome> queryResult = dynamoDBClient
                .queryIndex(table_entity, "authorId-tsId-index", col_authorId, authorId);


        final List<Entity> entities = new ArrayList<>();

        final Iterator<Item> iterator = queryResult.iterator();

        while (iterator.hasNext()) {
            final Item item = iterator.next();
            final Entity entity = Entity.newInstanceFromDynamoDbItem(item);
            entities.add(entity);
        }

        final Response response = new Response();
        response.setResponse(entities);

        return response;
    }

    @Override
    public void validateRequest() {
        assertNotBlank(authorId, "author Id");
    }
}
