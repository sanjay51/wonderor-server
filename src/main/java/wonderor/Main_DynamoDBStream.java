package wonderor;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.google.gson.Gson;
import wonderor.activities.EventHandlerFactory;

public class Main_DynamoDBStream implements RequestHandler<DynamodbEvent, String> {

    @Override
    public String handleRequest(DynamodbEvent event, Context context) {
        Gson gson = new Gson();

        System.out.println(gson.toJson(event));
        System.out.println(gson.toJson(context));

        for (DynamodbEvent.DynamodbStreamRecord record: event.getRecords()) {
            EventHandlerFactory.getInstance(record).handle();
        }

        return "Completed.";
    }
}